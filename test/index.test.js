// const mocha = require('mocha');
const assert = require('assert');
const sum = require('../index')

describe("Sum 2 numbers", () => {
    it("5+5 is 10", () => {
        assert.equal(10, sum(5, 5));
    });
    it("5+5 is not 55", () => {
        assert.notEqual(55, sum(5, 5));
    });
});