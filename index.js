const log4js = require('log4js');

const logger = log4js.getLogger();

logger.level = "info";
logger.debug("Initializing app in debug mode...");
logger.info("App initialized");
logger.warn("Config file is missing");
logger.error("Unauthorized access to System Files");
logger.fatal("App couldn't start :c");

function sum(x, y) {
    return x + y;
}

module.exports = sum;

var var1 = "HOLA";